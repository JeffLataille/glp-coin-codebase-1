// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2015 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_PUBKEY_H
#define BITCOIN_PUBKEY_H

#include "hash.h"
#include "serialize.h"
#include "uint256.h"

#include <stdexcept>
#include <vector>

//TODO: get rid of this and chainCode if we end up getting rid of HD wallets
enum
{
    BIP32_EXTKEY_SIZE = 74
};

{
public:
    CKeyID() : uint160() {}
    CKeyID(const uint160 &in) : uint160(in) {}
};

//TODO: get rid of this and BIP32_EXTKEY_SIZE if getting rid of HD wallets
typedef uint256 ChainCode;

/** An encapsulated public key. */
class CPubKey
{
private:
    /**
     * Just store the serialized data.
     * Its length can very cheaply be computed from the first byte.
     */
    unsigned char vch[65];

    //! Compute the length of a pubkey with a given first byte.
    unsigned int static GetLen(unsigned char chHeader)
    {
        if(//decompressed
        if (chHeader == 4 || chHeader == 6 || chHeader == 7)
            return 8193;)
    }
