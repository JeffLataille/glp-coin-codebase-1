// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2015 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_PUBKEY_H
#define BITCOIN_PUBKEY_H

#include "hash.h"
#include "serialize.h"
#include "uint256.h"
#include "glyph/glp.h"

#include <stdexcept>
#include <vector>

/**
 * secp256k1:
 * const unsigned int PRIVATE_KEY_SIZE = 279;
 * const unsigned int PUBLIC_KEY_SIZE  = 65;
 * const unsigned int SIGNATURE_SIZE   = 72;
 *
 * see www.keylength.com
 * script supports up to 75 for single byte push
 */

#define PK_SIZE 2176

enum
{
    BIP32_EXTKEY_SIZE = 74
};

/** A reference to a CKey: the Hash160 of its serialized public key */
class CKeyID : public uint160
{
public:
    CKeyID() : uint160() {}
    CKeyID(const uint160 &in) : uint160(in) {}
};

typedef uint256 ChainCode;

/** An encapsulated public key. */
class CPubKey
{
private:
    /**
     * Just store the serialized data.
     * Its length can very cheaply be computed from the first byte.
     */

    bool fValid;


public:
    unsigned char vch[PK_SIZE];


    //! Construct an invalid public key.
    CPubKey() { fValid=false; }
    //! Initialize a public key using begin/end iterators to byte data.
    template <typename T>
    void Set(const T pbegin, const T pend)
    {
        int len = pend == pbegin ? 0 : size();
        if (len && len == (pend - pbegin))
            memcpy(vch, (unsigned char *)&pbegin[0], len);
        else
            fValid = false;
    }
    //! Construct a public key using begin/end iterators to byte data.
    template <typename T>
    CPubKey(const T pbegin, const T pend)
    {
        Set(pbegin, pend);
        fValid = true;
    }

    //! Construct a public key from a byte vector.
    CPubKey(const std::vector<unsigned char> &vch) {
        Set(vch.begin(), vch.end());
        fValid = true;
    }

    //! Simple read-only vector-like interface to the pubkey data.
    unsigned int size() const { return PK_SIZE; }
    const unsigned char *begin() const { return vch; }
    const unsigned char *end() const { return vch + size(); }
    const unsigned char &operator[](unsigned int pos) const { return vch[pos]; }
    //! Comparator implementation.
    friend bool operator==(const CPubKey &a, const CPubKey &b)
    {
        return a.vch[0] == b.vch[0] && memcmp(a.vch, b.vch, a.size()) == 0;
    }
    friend bool operator!=(const CPubKey &a, const CPubKey &b) { return !(a == b); }
    friend bool operator<(const CPubKey &a, const CPubKey &b)
    {
        return a.vch[0] < b.vch[0] || (a.vch[0] == b.vch[0] && memcmp(a.vch, b.vch, a.size()) < 0);
    }

    //! Implement serialization, as if this was a byte vector.
    template <typename Stream>
    void Serialize(Stream &s) const
    {
        unsigned int len = size();
        ::WriteCompactSize(s, len);
        s.write((char *)vch, len);
    }
    template <typename Stream>
    void Unserialize(Stream &s)
    {
        unsigned int len = ::ReadCompactSize(s);
        if (len <= size())
        {
            s.read((char *)vch, len);
        }
        else
        {
            // invalid pubkey, skip available data
            char dummy;
            while (len--)
                s.read(&dummy, 1);
        }
    }

    //! Get the KeyID of this public key (hash of its serialization)
    CKeyID GetID() const { return CKeyID(Hash160(vch, vch + size())); }
    //! Get the 256-bit hash of this public key.
    uint256 GetHash() const { return Hash(vch, vch + size()); }
    /*
     * Check syntactic correctness.
     *
     * Note that this is consensus critical as CheckSig() calls it!
     */
    bool IsValid() const { return fValid;}
    //! fully validate whether this is a valid public key (more expensive than IsValid())
    bool IsFullyValid() const;

    void getPKStruct(glp_public_key_t * pkStruct) const;

    void structToPK(glp_public_key_t * pkStruct);

    bool Verify(const uint256 &hash, const std::vector<unsigned char> &vchSig) const;

    // /**
    //  * Check whether a signature is normalized (lower-S).
    //  */
    // static bool CheckLowS(const std::vector<unsigned char> &vchSig);
    //
    // //! Recover a public key from a compact signature.
    // bool RecoverCompact(const uint256 &hash, const std::vector<unsigned char> &vchSig);
    //
    // //! Turn this public key into an uncompressed public key.
    // bool Decompress();
    //
    // //! Derive BIP32 child pubkey.
    // bool Derive(CPubKey &pubkeyChild, ChainCode &ccChild, unsigned int nChild, const ChainCode &cc) const;


};
//
// struct CExtPubKey
// {
//     unsigned char nDepth;
//     unsigned char vchFingerprint[4];
//     unsigned int nChild;
//     ChainCode chaincode;
//     CPubKey pubkey;
//
//     friend bool operator==(const CExtPubKey &a, const CExtPubKey &b)
//     {
//         return a.nDepth == b.nDepth && memcmp(&a.vchFingerprint[0], &b.vchFingerprint[0], 4) == 0 &&
//                a.nChild == b.nChild && a.chaincode == b.chaincode && a.pubkey == b.pubkey;
//     }
//
//     void Encode(unsigned char code[BIP32_EXTKEY_SIZE]) const;
//     void Decode(const unsigned char code[BIP32_EXTKEY_SIZE]);
//     bool Derive(CExtPubKey &out, unsigned int nChild) const;
//
//     void Serialize(CSizeComputer &s) const
//     {
//         // Optimized implementation for ::GetSerializeSize that avoids copying.
//         s.seek(BIP32_EXTKEY_SIZE + 1); // add one byte for the size (compact int)
//     }
//     template <typename Stream>
//     void Serialize(Stream &s) const
//     {
//         unsigned int len = BIP32_EXTKEY_SIZE;
//         ::WriteCompactSize(s, len);
//         unsigned char code[BIP32_EXTKEY_SIZE];
//         Encode(code);
//         s.write((const char *)&code[0], len);
//     }
//     template <typename Stream>
//     void Unserialize(Stream &s)
//     {
//         unsigned int len = ::ReadCompactSize(s);
//         unsigned char code[BIP32_EXTKEY_SIZE];
//         if (len != BIP32_EXTKEY_SIZE)
//             throw std::runtime_error("Invalid extended key size\n");
//         s.read((char *)&code[0], len);
//         Decode(code);
//     }
// };
//
#endif
