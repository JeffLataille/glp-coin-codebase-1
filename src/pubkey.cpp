// Copyright (c) 2009-2015 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <glyph/converter.h>
#include "pubkey.h"
#include "util.h"
#include "string.h"

#include <secp256k1.h>
#include <secp256k1_recovery.h>

extern "C" void PKStructToPKByte(RINGELT pk[GLP_N], unsigned char cPK[2176]);
extern "C" void PKByteToPKStruct(RINGELT pk[GLP_N], const unsigned char cPK[2176]);


namespace
{
/* Global secp256k1_context object used for verification. */
secp256k1_context* secp256k1_context_verify = NULL;
}
static bool vectorToSig(glp_signature_t * sigStruct, const unsigned char * vchSig, int * indexPtr)  {
  //convert arraySig to struct sig
  byteSigToZ1(sigStruct->z1, vchSig, indexPtr);
  byteSigToZ2(sigStruct->z2, (vchSig+ *indexPtr), indexPtr);
  byteSigToSparse(&(sigStruct->c), (vchSig+ *indexPtr), indexPtr);

  return true;
}

void CPubKey::getPKStruct(glp_public_key_t * pkStruct) const {
  // //glp_signing_key_t * newSK = (glp_signing_key_t *)calloc(1, sizeof(glp_signing_key_t));
  // glp_public_key_t * newPK = (glp_public_key_t * )calloc(1, sizeof(glp_public_key_t));
  // //glp_signature_t * newSig = (glp_signature_t * ) calloc(1, sizeof(glp_signature_t));
  // //free(newSK);
  // free(newPK);
  // //free(newSig);

  PKByteToPKStruct(pkStruct->t, vch);
}

void CPubKey::structToPK(glp_public_key_t * pkStruct) {

  // //glp_signing_key_t * newSK = (glp_signing_key_t *)calloc(1, sizeof(glp_signing_key_t));
  // glp_public_key_t * newPK = (glp_public_key_t * )calloc(1, sizeof(glp_public_key_t));
  // //glp_signature_t * newSig = (glp_signature_t * ) calloc(1, sizeof(glp_signature_t));
  // //free(newSK);
  // free(newPK);
  // //free(newSig);

  for(int i = 0; i < 2176; i++) {
      vch[i] = 0;
  }
  PKStructToPKByte(pkStruct->t, vch);
  fValid = true;
}


bool CPubKey::Verify(const uint256 &hash, const std::vector<unsigned char> &vchSig) const
{
    if (!IsValid()) {
        printf("IsValid failed in Verify pubkey.cpp\n");
        return false;

    }

    // //TODO: when I have these callocs and free for 3 structs that are used in glyph, I get no errors.
    // //When any one is commented out I get errors
    // //glp_signing_key_t * newSK = (glp_signing_key_t *)calloc(1, sizeof(glp_signing_key_t));
    // glp_public_key_t * newPK = (glp_public_key_t * )calloc(1, sizeof(glp_public_key_t));
    // glp_signature_t * newSig = (glp_signature_t * ) calloc(1, sizeof(glp_signature_t));
    // //free(newSK);
    // free(newPK);
    // free(newSig);

    //convert hash to string hashStr
    unsigned char hashData[32];
    memcpy(hashData, hash.begin(), 32);

    //decompress signature
    glp_signature_t * sigStruct = (glp_signature_t *)calloc(1, sizeof(glp_signature_t));
    int index = 0;
    int * indexPtr = &index;
    //const std::vector<unsigned char> * vchSigPtr = &vchSig;
    vectorToSig(sigStruct, &vchSig[0], indexPtr);

    glp_public_key_t * pkStruct = (glp_public_key_t *)calloc(1, sizeof(glp_public_key_t));
    getPKStruct(pkStruct);
    //Verify
    if(!glp_verify(*sigStruct, *pkStruct, hashData, 32)){
      printf("pubkey.cpp: signature verify failed\n");
      LogPrintf("pubkey.cpp: signature verify failed");
      return false;
    }
    free(sigStruct);
    free(pkStruct);
    return true;

}

bool CPubKey::IsFullyValid() const
{
    //check if each int of CPubKey is between 0 and prime value.

    return IsValid(); //delete once you implement this function fully (as described above)

//     if (!IsValid())
//         return false;
//     secp256k1_pubkey pubkey;
//     return secp256k1_ec_pubkey_parse(secp256k1_context_verify, &pubkey, &(*this)[0], size());
}




// bool CPubKey::RecoverCompact(const uint256 &hash, const std::vector<unsigned char> &vchSig)
// {
//     if (vchSig.size() != 65)
//         return false;
//     int recid = (vchSig[0] - 27) & 3;
//     bool fComp = ((vchSig[0] - 27) & 4) != 0;
//     secp256k1_pubkey pubkey;
//     secp256k1_ecdsa_recoverable_signature sig;
//     if (!secp256k1_ecdsa_recoverable_signature_parse_compact(secp256k1_context_verify, &sig, &vchSig[1], recid))
//     {
//         return false;
//     }
//     if (!secp256k1_ecdsa_recover(secp256k1_context_verify, &pubkey, &sig, hash.begin()))
//     {
//         return false;
//     }
//     unsigned char pub[65];
//     size_t publen = 65;
//     secp256k1_ec_pubkey_serialize(
//         secp256k1_context_verify, pub, &publen, &pubkey, fComp ? SECP256K1_EC_COMPRESSED : SECP256K1_EC_UNCOMPRESSED);
//     Set(pub, pub + publen);
//     return true;
// }

//
// bool CPubKey::Decompress()
// {
// //     if (!IsValid())
// //         return false;
// //     secp256k1_pubkey pubkey;
// //     if (!secp256k1_ec_pubkey_parse(secp256k1_context_verify, &pubkey, &(*this)[0], size()))
// //     {
// //         return false;
// //     }
// //     unsigned char pub[65];
// //     size_t publen = 65;
// //     secp256k1_ec_pubkey_serialize(secp256k1_context_verify, pub, &publen, &pubkey, SECP256K1_EC_UNCOMPRESSED);
// //     Set(pub, pub + publen);
// //     return true;
//     return true;
// }
//
// bool CPubKey::Derive(CPubKey &pubkeyChild, ChainCode &ccChild, unsigned int nChild, const ChainCode &cc) const
// {
//     assert(IsValid());
//     assert((nChild >> 31) == 0);
//     assert(begin() + 33 == end());
//     unsigned char out[64];
//     BIP32Hash(cc, nChild, *begin(), begin() + 1, out);
//     memcpy(ccChild.begin(), out + 32, 32);
//     secp256k1_pubkey pubkey;
//     if (!secp256k1_ec_pubkey_parse(secp256k1_context_verify, &pubkey, &(*this)[0], size()))
//     {
//         return false;
//     }
//     if (!secp256k1_ec_pubkey_tweak_add(secp256k1_context_verify, &pubkey, out))
//     {
//         return false;
//     }
//     unsigned char pub[33];
//     size_t publen = 33;
//     secp256k1_ec_pubkey_serialize(secp256k1_context_verify, pub, &publen, &pubkey, SECP256K1_EC_COMPRESSED);
//     pubkeyChild.Set(pub, pub + publen);
//     return true;
// }
//
// void CExtPubKey::Encode(unsigned char code[BIP32_EXTKEY_SIZE]) const
// {
//     code[0] = nDepth;
//     memcpy(code + 1, vchFingerprint, 4);
//     code[5] = (nChild >> 24) & 0xFF;
//     code[6] = (nChild >> 16) & 0xFF;
//     code[7] = (nChild >> 8) & 0xFF;
//     code[8] = (nChild >> 0) & 0xFF;
//     memcpy(code + 9, chaincode.begin(), 32);
//     assert(pubkey.size() == 33);
//     memcpy(code + 41, pubkey.begin(), 33);
// }
//
// void CExtPubKey::Decode(const unsigned char code[BIP32_EXTKEY_SIZE])
// {
//     nDepth = code[0];
//     memcpy(vchFingerprint, code + 1, 4);
//     nChild = (code[5] << 24) | (code[6] << 16) | (code[7] << 8) | code[8];
//     memcpy(chaincode.begin(), code + 9, 32);
//     pubkey.Set(code + 41, code + BIP32_EXTKEY_SIZE);
// }
//
// bool CExtPubKey::Derive(CExtPubKey &out, unsigned int nChild) const
// {
//     out.nDepth = nDepth + 1;
//     CKeyID id = pubkey.GetID();
//     memcpy(&out.vchFingerprint[0], &id, 4);
//     out.nChild = nChild;
//     return pubkey.Derive(out.pubkey, out.chaincode, nChild, chaincode);
// }
//
// /* static */ bool CPubKey::CheckLowS(const std::vector<unsigned char> &vchSig)
// {
//     // secp256k1_ecdsa_signature sig;
//     // if (!ecdsa_signature_parse_der_lax(secp256k1_context_verify, &sig, &vchSig[0], vchSig.size()))
//     // {
//     //     return false;
//     // }
//     // return (!secp256k1_ecdsa_signature_normalize(secp256k1_context_verify, NULL, &sig));
//     return true;
// }
