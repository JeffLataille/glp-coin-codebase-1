/* This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * See LICENSE for complete information.
 */


#include "glp.h"
#include "glp_utils.h"
#include "converter.h"
#define SIGN_TRIALS 10000


int newSigTest() {
  glp_signing_key_t sk;
  glp_public_key_t pk;
  char *message = "testtesttestest";
  glp_signature_t sig;
  //glp_signature_t newSig;

  uint16_t i;

  int index = 0;
  int * indexPtr1 = &index;
  int index2 = 0;
  int * indexPtr2 = &index2;

  printf("******************************************************************************************************\n");

  /*test a lot of verifications*/
  printf("trying %d independent keygen/sign/verifies\n", SIGN_TRIALS);
  for(i=0; i < SIGN_TRIALS; i++){
    glp_signature_t * newSig = (glp_signature_t * ) calloc(1, sizeof(glp_signature_t));
    glp_signing_key_t * newSK = (glp_signing_key_t *)calloc(1, sizeof(glp_signing_key_t));
    glp_public_key_t * newPK = (glp_public_key_t * )calloc(1, sizeof(glp_public_key_t));
    glp_gen_sk(&sk);
    unsigned char * byteSK = (unsigned char*)calloc(1, 512);
    glp_gen_pk(&pk,sk);
    SKStructToSKByte(&sk, byteSK);
    SKByteToSKStruct(newSK, byteSK);

    int index = 0;
    int * indexPtr1 = &index;
    int index2 = 0;
    int * indexPtr2 = &index2;
    if(!glp_sign(&sig, *newSK,(unsigned char *)message,strlen(message))){
      printf("signature failure round %d!\n",i);
    }
    for(int j = 0; j < 1024; j++) {
   //     printf("%d:%lu\n", j, sig.z1[j]);
    }

    //testing pk convert
    unsigned char * cPK = (unsigned char * ) calloc(1, 2176);
    //pk.t[0] = 7284;
    //pk.t[1] = 30096;
   // int temp = pk.t[0];
   // temp++;
   // temp--;
   // pk.t[0] = temp;
    PKStructToPKByte(pk.t, cPK);
    PKByteToPKStruct(newPK->t, cPK);


   // printf("\n affter\n");
    for(int j = 0; j < 1024; j++) {
//        printf("%d:%lu\n", j, pk.t[j]);
    }

    //unsigned char * byteSig = calloc(1, 2198);
    unsigned char * byteSig = calloc(1, 2454);

    sigToVector(sig, byteSig, indexPtr1);
    vectorToSig(newSig, byteSig, indexPtr2);

    int a = memcmp(pk.t, newPK->t, 8192);
    int b = memcmp(sig.z1, newSig->z1, 8192);
    int c = memcmp(sig.z2, newSig->z2, 8192);
    int d = memcmp(sig.c.pos, newSig->c.pos, 32);
    int e = memcmp(sig.c.sign, newSig->c.sign, 32);
    int f = memcmp(sk.s1, newSK->s1, 8192);
    int g = memcmp(sk.s2, newSK->s2, 8192);

//    printf("\n printing both\n");
//        for(int i = 0; i < 1024; i++) {
 //             printf("pk %d: pk1:%lu new:%lu\n", i, pk.t[i], newPK->t[i]);
//        }
//        printf("conversion error: pk:%d z1:%d z2:%d pos:%d sign:%d sk.s1:%d sk.s2:%d\n", a, b, c, d, e, f, g);
//    printf("\n\n");
    if(a != 0 || b != 0 || c != 0 || d != 0 || e != 0 || f != 0 || g != 0) {
        printf("conversion error: pk:%d z1:%d z2:%d pos:%d sign:%d sk.s1:%d sk.s2:%d\n", a, b, c, d, e, f, g);
        for(int i = 0; i < 1024; i++) {
            if(sig.z1[i] != newSig->z1[i]) {
              printf("pk %d: pk1:%lu new:%lu\n", i, sig.z1[i], newSig->z1[i]);
            }
        }

    }

    if(!glp_verify(*newSig, *newPK,(unsigned char *)message,strlen(message))){
      printf("verification failure round %d!\n",i);
      return 1;
    }
    if(!(i % 100)){
      printf("passed trial %d\n",i);
    }
  }
  printf("signature scheme validates across %d independent trials\n", SIGN_TRIALS);
  printf("******************************************************************************************************\n");


  return 0;
}

int normalTest() {
  glp_signing_key_t sk;
  glp_public_key_t pk;
  char *message = "testtesttestest";
  glp_signature_t sig;

  unsigned int maxBig = 0;
  unsigned int minBig = GLP_Q;
  unsigned int maxSmall = 0;
  unsigned int minSmall = GLP_Q;

  uint16_t i;

  printf("******************************************************************************************************\n");

  /*test a lot of verifications*/
  printf("trying %d independent keygen/sign/verifies\n", SIGN_TRIALS);
  for(i=0; i < SIGN_TRIALS; i++){
    glp_gen_sk(&sk);
    glp_gen_pk(&pk,sk);
    if(!glp_sign(&sig, sk,(unsigned char *)message,strlen(message))){
      printf("signature failure round %d!\n",i);
    }

    //testingConverter(pk, sig, sk);
    //testVectorToSig(sig);

    // for(int i = 0; i < 1024; i++) {
    //     if(sig.z1[i] > maxBig) { maxBig = sig.z1[i];}
    //     else if(sig.z1[i] < minSmall) { minSmall = sig.z1[i]; }
    //
    //     if(sig.z1[i] > maxSmall && sig.z1[i] < 20000) {
    //         maxSmall = sig.z1[i];
    //     }
    //     else if(sig.z1[i] < minBig && sig.z1[i] > 20000) {
    //         minBig = sig.z1[i];
    //     }
    // }

    if(!glp_verify(sig, pk,(unsigned char *)message,strlen(message))){
      printf("verification failure round %d!\n",i);
      return 1;
    }
    if(!(i % 100)){
      printf("passed trial %d\n",i);
    }
  }
  printf("max %d min %d" , maxBig, minBig);
  printf("max %d min %d" , maxSmall, minSmall);
  printf("signature scheme validates across %d independent trials\n", SIGN_TRIALS);
  printf("******************************************************************************************************\n");


  return 0;
}

int skTest(){
  for(int i = 0; i < 10; i++) {
    glp_signing_key_t sk;
    glp_gen_sk(&sk);
    unsigned char * cSK = (unsigned char*)calloc(1, 512);
    SKStructToSKByte(&sk, cSK);
    glp_signing_key_t sk2;
    SKByteToSKStruct(&sk2, cSK);
    int s1 = memcmp(sk.s1, sk2.s1, 8192);
    int s2 = memcmp(sk.s2, sk2.s2, 8192);
    printf("s1:%d, s2:%d\n", s1, s2);
    free(cSK);
  }
  return 1;
}

int main(){

  //newSigTest();
  //skTest();
  //normalTest();


  return 0;
}
