#ifndef CONVERTER_H
#define CONVERTER_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_SIG_SIZE 2198

#include "glp.h"


void PKStructToPKByte(RINGELT pk[GLP_N], unsigned char cPK[2176]);

void PKByteToPKStruct(RINGELT pk[GLP_N], const unsigned char cPK[2176]);

void SKStructToSKByte (glp_signing_key_t * skStruct, unsigned char sk[512]);

void SKByteToSKStruct (glp_signing_key_t * skStruct, const unsigned char sk[512]);

void z2ToByteSig(RINGELT z2[GLP_N], unsigned char cZ2[256], int * indexPtr);

void byteSigToZ2(RINGELT z2[GLP_N], const unsigned char cZ2[256], int * indexPtr);

void byteSigToZ1(RINGELT z1[GLP_N], const unsigned char cZ1[1920], int * indexPtr);

void z1ToByteSig(RINGELT z1[GLP_N], unsigned char cZ1[1920], int * indexPtr);

void sparseToByteSig(sparse_poly_t * c, unsigned char cc[22], int * indexPtr);

void byteSigToSparse(sparse_poly_t * c, const unsigned char cc[22], int * indexPtr);

#ifdef __cplusplus
}
#endif

#endif
