/* This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * See LICENSE for complete information.
 */


#include "glp_utils.h"
#include "glp_rand_openssl_aes.h"


/**************************************************************PRINTING AND COPYING******************************************************/

void copy_poly(RINGELT f[GLP_N], const RINGELT g[GLP_N]){
  uint16_t i;
  for(i = 0; i < GLP_N; i++) f[i] = g[i];
}

void print_poly(const RINGELT f[GLP_N]){
  uint16_t i;
  for(i = 0; i < GLP_N ; i++){
    //printf("%ld ", 2*f[i] < Q ? f[i] : f[i] - Q );
    printf("%"PRIdFAST64" ", 2*f[i] < q_ringelt ? f[i] : f[i] - q_ringelt );
  }
  printf("\n");
}


void print_sk(const glp_signing_key_t sk){
  printf("s1:");
  print_poly(sk.s1);
  printf("\n");
  printf("s2:");
  print_poly(sk.s2);
}


void print_pk(const glp_public_key_t pk){
  printf("t:");
  print_poly(pk.t);
}

void print_sparse(const sparse_poly_t s){
  uint16_t i;
  for(i = 0; i < OMEGA; i++) printf("(%d,%d)",s.pos[i],s.sign[i]);
  printf("\n");
}

void print_sig(const glp_signature_t sig){
  printf("z1:");
  print_poly(sig.z1);
  printf("\n");
  printf("z2:");
  print_poly(sig.z2);
  printf("\n");
  printf("c:");
  print_sparse(sig.c);
  printf("\n");
}



/***************************************************************HASH ROUTINE*************************************/
void poly2bytes(unsigned char *y, RINGELT f[GLP_N]){
  RINGELT x;
  uint16_t i,j;
  for(i = 0; i < GLP_N; i++){
    x = f[i];
#if !NISPOWEROFTWO
    if (i == GLP_N-1)
      x = 0;
#endif
    for (j = 0; j < 2; j++){
      y[2*i + j] = x & 0xff;
      x >>= 8;
    }
    /*
    for(j = 0; j < Q_BYTES; j++){
      y[Q_BYTES*i + j] = x & 0xff;
      x >>= 8;
    }
    */
  }
}

/*hash function */
/*input: one polynomial, mu (usually itself a message digest),
         and the length of mu in bytes*/
/*output: a 256-bit hash */

int hash(unsigned char hash_output[GLP_DIGEST_LENGTH],
	  RINGELT u[GLP_N],
	  const unsigned char* mu,
	  const size_t mulen){
  int ret;

  #if NISPOWEROFTWO
  uint64_t bytesPerPoly = GLP_N * 2;
  #else
  uint64_t bytesPerPoly = (GLP_N-1) * Q_BYTES;
  #endif

  uint64_t hash_input_bytes = bytesPerPoly+ mulen;
  uint16_t i;
  unsigned char *hash_input;
  if ((hash_input = (unsigned char *)malloc(hash_input_bytes)) == NULL) return 0;
//memset(hash_input, 0, hash_input_bytes);
  poly2bytes(hash_input,u);
  for(i = 0; i < mulen; i++) hash_input[i + bytesPerPoly] = mu[i];
  SHA256_CTX sha256;
  ret = 1;
  ret &= SHA256_Init(&sha256);
  ret &= SHA256_Update(&sha256, hash_input, hash_input_bytes);
  ret &= SHA256_Final(hash_output, &sha256);
  free(hash_input);
  return ret;
}




/***************************************************************SPARSE POLY MULTIPLICATION*************************/
void sparse_mul(RINGELT v[GLP_N], const RINGELT a[GLP_N], const sparse_poly_t b){

  RINGELT v_aux[2*GLP_N];
  uint16_t i,j;
  /*zero the output*/
  for(i = 0; i < 2*GLP_N; i++) v_aux[i] = 0;

  /*multiply in Z[x]*/
  for(i = 0; i < OMEGA; i++) {
    for(j = 0; j < GLP_N; j++) {
      if(b.sign[i] == 1) ADD_MOD(v_aux[b.pos[i] + j], v_aux[b.pos[i] + j], a[j],GLP_Q);
      else SUB_MOD(v_aux[b.pos[i] + j], v_aux[b.pos[i] + j], a[j],GLP_Q);
    }
  }
#if NISPOWEROFTWO
  /*reduce mod x^n + 1*/
  for(i = 0; i < GLP_N; i++) SUB_MOD(v[i], v_aux[i], v_aux[i+GLP_N], GLP_Q);
#else
  /*reduce mod x^n - 1*/
  for(i = 0; i < GLP_N; i++) ADD_MOD(v[i], v_aux[i], v_aux[i+GLP_N], GLP_Q);
#endif
}

/**************************************************************ENCODE ROUTINES*************************************/
// Given hash_output=Hash(w,m), converts to c(x)=PolyHash(w, m), where c(x) is a sparse polynomial -- e.g.,
// c:(396,1)(185,1)(565,1)(539,1)(256,1)(156,1)(417,0)(287,1)(54,1)(1009,0)(413,1)(652,1)(938,0)(723,1)(300,0)(357,0)
// where the sixteen pairs (pos, sign) specify the non-zero positions and coeffs (0=>1; 1=>-1)

int encode_sparse(sparse_poly_t *encode_output,
                  const unsigned char hash_output[GLP_DIGEST_LENGTH]){

  // the following AES code is essentially the same as calling RANDOM_VARS, except that the key size is now 256 bits, and hash_output[] rather than aes_key_bytes[] is used as the key
  // seed AES with hash output

  // define aes_key struct
  /*
  struct aes_key_st {
    uint32_t rd_key[4*(AES_MAXNR+1)]; // AES_MAXNR=14 is max num of rounds
    unsigned rounds;
  };
  typedef struct aes_key_st AES_KEY;
   */

  AES_KEY aes_key;

  // AES_set_encrypt_key(hash_output, 8*GLP_DIGEST_LENGTH, aes_key)
  // configures aes_key to encrypt with the 8*GLP_DIGEST_LENGTH (=8*32=256)-bit key hash_output;
  // Note: hash_output is the actual 256-bit key;
  // Note: aes_key is a data structure holding a transformed version of the actual key, for efficiency;
  // Note: Unlike some other OpenSSL functions, AES_set_encrypt_key returns zero on success and a negative number on error;

  if (AES_set_encrypt_key(hash_output,8*GLP_DIGEST_LENGTH,&aes_key) < 0){
    return 0;
  }

  // initialise AES

  unsigned char aes_ivec[AES_BLOCK_SIZE]; // AES_BLOCK_SIZE is 16
  memset(aes_ivec, 0, AES_BLOCK_SIZE);

  unsigned char aes_ecount_buf[AES_BLOCK_SIZE];
  memset(aes_ecount_buf, 0, AES_BLOCK_SIZE);

  unsigned int aes_num = 0;

  unsigned char aes_in[AES_BLOCK_SIZE];
  memset(aes_in, 0, AES_BLOCK_SIZE);

  // get OMEGA values in [0,n), each with a 0 or 1 to indicate sign
  uint64_t rand64 = randomplease(&aes_key, aes_ivec, aes_ecount_buf, &aes_num, aes_in);
  uint16_t rand_bits_used = 0;
  uint16_t pos,sign,i,j;
  uint64_t mask=~((~0u)<<NBITS); // mask = 1111111111_2

  for(i=0; i<OMEGA; i++){
    while(1){
      // make sure we have enough bits left in rand64; recall we need 11
      if(rand_bits_used > (64 - (1+NBITS))){ // recall: NBITS is 10, because N=1024=2^{10} is ten bits long
        rand64 = randomplease(&aes_key, aes_ivec, aes_ecount_buf, &aes_num, aes_in);
	rand_bits_used = 0;
      }

      /*get random bits for this coefficient */
      sign = rand64&1;     // sign determined by lsb of rand64
      rand64 >>= 1;        // right shift off the lsb of rand64
      rand_bits_used += 1; // increment by one to account for usage of sign bit
      pos = rand64 & mask; // grab the ten lsb's of rand64; they form a ten bit number 0 <= pos <= 1023 = n-1
      rand64 >>= NBITS;    // right shift off the ten lsb's of rand64
      rand_bits_used += NBITS; // increment by 10 to account for usage of ten positional bits

      /*get position from random*/
#if NISPOWEROFTWO
      if(pos < GLP_N)
#else
      if(pos < GLP_N-1)
#endif
        {
          // check we are not using this position already
          int success=1;
	  // check only those selections j made prior to the current selection i
          for(j = 0; j < i; j++){
	    if (pos == encode_output->pos[j]){
	      success = 0;
	    }
	  }
	  // if current selected position has not yet been used, then break while loop
          if(success){
	    break;
	  }
        }
    }
    (encode_output->sign)[i] = sign; // store sign at newly selected position
    (encode_output->pos)[i] = pos;   // store newly selected position
  }
  return 1;
}


/***********************************************ROUNDING***********************/
void round_poly(RINGELT f[GLP_N], RINGELT K){
  uint16_t i;
  for(i = 0; i < GLP_N; i++){
    f[i] = high_bits(f[i],K);
  }
}

/*high bits of z*/
RINGELT high_bits(RINGELT z, RINGELT K){
  RINGELT q, r;

  q=z/(RINGELT)(2*K + 1);
  r=z-q*(2*K+1);
  if (r > K){
    q+=1;
  }
  return q;
}

/*low bits of z, represented in [-K,K) mod Q */
RINGELT low_bits(RINGELT z, RINGELT K){
  RINGELT q, r;

  q=z/(RINGELT)(2*K+1);
  r=z-q*(2*K+1);
  if (r > K){
    r+=GLP_Q-(2*K+1);
  }
  return r;
}
