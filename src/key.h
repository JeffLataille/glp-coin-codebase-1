// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2015 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_KEY_H
#define BITCOIN_KEY_H

#include "glyph/converter.h"
#include <glyph/glp.h>
#include "pubkey.h"
#include "serialize.h"
#include "support/allocators/secure.h"
#include "uint256.h"
#include <stdexcept>
#include <vector>
#include <unistd.h>

//TODO: prolly get rid of this
#define SK_SIZE 512 //the theoretical limit is 2*N*log_2(3). N is 1024.

/**
 * secure_allocator is defined in allocators.h
 * CPrivKey is a serialized private key, with all parameters included (279 bytes)
 */
typedef std::vector<unsigned char, secure_allocator<unsigned char> > CPrivKey;


/** An encapsulated private key. */
class CKey
{
private:
    //  Whether this private key is valid. We check for correctness when modifying the key
    //  data, so fValid should always correspond to the actual state.
    bool fValid;

    unsigned char sk[SK_SIZE];

public:

    //  Construct an invalid private key.
    CKey() : fValid(false) {
      LockObject(sk);
     }
    //  Copy constructor. This is necessary because of memlocking.
    CKey(const CKey &secret) : fValid(secret.fValid)
    {
        LockObject(sk);
        memcpy(sk, secret.begin(), SK_SIZE);
    }

    //  Destructor (again necessary because of memlocking).
    ~CKey() { UnlockObject(sk);
     }
    friend bool operator==(const CKey &a, const CKey &b)
    {
        return a.size() == b.size() && memcmp(&a.sk[0], &b.sk[0], a.size()) == 0;
    }

    //  Initialize using begin and end iterators to byte data.
    template <typename T>
    void Set(const T pbegin, const T pend)
    {
        if (pend - pbegin != SK_SIZE)
        {
            fValid = false;
            return;
        }
        else
        {

            memcpy(sk, (unsigned char *)&pbegin[0], SK_SIZE);
            fValid = true;
        }

    }
    //  Simple read-only vector-like interface.
    unsigned int size() const { return SK_SIZE; }
    const unsigned char *begin() const { return sk; }
    const unsigned char *end() const { return sk + size(); }
    //  Check whether this private key is valid.
    bool IsValid() const { return fValid; }

    //  Generate a new private key using a cryptographic PRNG.
    void MakeNewKey();

    /**
     * Convert the private key to a CPrivKey (serialized OpenSSL private key data).
     * This is expensive.
     */
    CPrivKey GetPrivKey() const;

    /**
     * Compute the public key from a private key.
     * This is expensive.
     */
    CPubKey GetPubKey() const;

    /**
     * Create a DER-serialized signature.
     * The test_case parameter tweaks the deterministic nonce.
     */
    bool Sign(const uint256 &hash, std::vector<unsigned char> &vchSig, uint32_t test_case = 0) const;

    /**
     * Verify thoroughly whether a private key and a public key match.
     * This is done by test signing it rather than regenerating it.
     */
    bool VerifyPubKey(const CPubKey &vchPubKey) const;

    //  Load private key and check that public key matches.
    bool Load(CPrivKey &privkey, CPubKey &vchPubKey, bool fSkipCheck);

};

#endif // BITCOIN_KEY_H
